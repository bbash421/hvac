import settings


def brush_on():
	settings.GPIO_BRUSH_TOP.output(settings.BRUSH_MOTOR_TOP_1, settings.GPIO_BRUSH_TOP.HIGH)
	settings.GPIO_BRUSH_TOP.output(settings.BRUSH_MOTOR_TOP_2, settings.GPIO_BRUSH_TOP.LOW)
	settings.GPIO_BRUSH_BOTTOM.output(settings.BRUSH_MOTOR_BOTTOM_1, settings.GPIO_BRUSH_BOTTOM.HIGH)
	settings.GPIO_BRUSH_BOTTOM.output(settings.BRUSH_MOTOR_BOTTOM_2, settings.GPIO_BRUSH_BOTTOM.LOW)
	return 'true'

def brush_off():
	settings.GPIO_BRUSH_TOP.output(settings.BRUSH_MOTOR_TOP_1, settings.GPIO_BRUSH_TOP.LOW)
	settings.GPIO_BRUSH_TOP.output(settings.BRUSH_MOTOR_TOP_2, settings.GPIO_BRUSH_TOP.LOW)
	settings.GPIO_BRUSH_BOTTOM.output(settings.BRUSH_MOTOR_BOTTOM_1, settings.GPIO_BRUSH_BOTTOM.LOW)
	settings.GPIO_BRUSH_BOTTOM.output(settings.BRUSH_MOTOR_BOTTOM_2, settings.GPIO_BRUSH_BOTTOM.LOW)
	return 'true'
