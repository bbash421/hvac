import settings
import time
import mysql.connector

def sensors():
       
        mydb=mysql.connector.connect(
            host='localhost',
                user='bryan',
                passwd='bryan',
                database='fau'
          )            

        #check the sensors
        while True:
            time.sleep(1)
            distfront = settings.distancefront()
            distleft = settings.distanceleft()
            distright = settings.distanceright()
            distdown = settings.distancedown()
            distback = settings.distanceback()
            
            distright = 30
            distleft = 30

            mycursorstatus = mydb.cursor()
            mycursorstatus.execute("SELECT sensorfront,sensorleft,sensorright,sensordown,sensorback from sensors")
            myresult = mycursorstatus.fetchall()

            if(myresult[0][0] + myresult[0][1] + myresult[0][2] + myresult[0][3] + myresult[0][4] > 20000):
                xerror = abs(((distfront-myresult[0][0])/myresult[0][0])*100)
                if(xerror >= 6):
                    distfront = myresult[0][0]

                xerror = abs(((distright-myresult[0][2])/myresult[0][2])*100)
                if(xerror >= 6):
                    distright = myresult[0][2]

                xerror = abs(((distback-myresult[0][4])/myresult[0][4])*100)
                if(xerror >= 6):
                    distback = myresult[0][4]

            #motors
            mycursorfwd = mydb.cursor()
            mycursorfwd.execute("SELECT direction from motor limit 0,1")
            myresult = mycursorfwd.fetchall()


            #if (myresult[0][0] != 'ST'):

            mycursorfwd = mydb.cursor()
            sql = "update sensors  set sensorfront=" + str(distfront) + ",sensorleft=" + str(distleft) + ",sensorright=" + str(distright) + ",sensordown=" + str(distdown) + ",sensorback=" + str(distback) + ";"
            mycursorfwd.execute(sql)
            mydb.commit()

            if(myresult[0][0] == 'FW'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.HIGH)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'BW'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.LOW)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'LF'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.LOW)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'RF'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.HIGH)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'LB'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.LOW)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1,settings.GPIO_RIGHT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'RB'):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.LOW)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.HIGH)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.LOW)

            if(myresult[0][0] == 'ST') or (myresult[0][0] == 'FW' and (distfront <= 10 or distleft <= 10 or distright <= 10 or distdown >= 5)) or (myresult[0][0] == 'LF' and (distfront <= 10 or distleft <= 10 or distdown >= 5)) or (myresult[0][0] == 'RF' and (distfront <= 10 or distright <= 10 or distdown >= 5)) or (myresult[0][0] == 'BW' and (distback <= 15 or distleft <= 10 or distright <= 10)) or  (myresult[0][0] == 'LB' and (distback <= 10 or distright <= 10)) or (myresult[0][0] == 'RB' and (distback <=10 or distleft <= 10)):
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_1, settings.GPIO_LEFT.LOW)
                settings.GPIO_LEFT.output(settings.MOTOR_LEFT_2, settings.GPIO_LEFT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_1, settings.GPIO_RIGHT.LOW)
                settings.GPIO_RIGHT.output(settings.MOTOR_RIGHT_2, settings.GPIO_RIGHT.LOW)

                mycursorupd = mydb.cursor()
                sql = "UPDATE motor SET direction='ST',driving=0"
                mycursorupd.execute(sql)
                mydb.commit()
                mycursorupd.close()



            if (myresult[0][0] != 'ST'):
                mycursorinsert = mydb.cursor()
                sql = "insert into traject (direction,date_traject,sensorfront,sensorleft,sensorright,sensordown,sensorback) values ('" + myresult[0][0] + "',now()," + str(distfront) + "," + str(distleft) + "," + str(distright) + "," + str(distdown) + "," + str(distback) + ")"
                mycursorinsert.execute(sql)
                mydb.commit()
                mycursorinsert.close()


        mydbfwd.close()
        mydb.close
        return "true"
        


