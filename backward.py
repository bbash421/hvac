def backward():
    import time
    import settings
    import mysql.connector

    mydb=mysql.connector.connect(
            host='localhost',
                user='bryan',
                passwd='bryan',
                database='fau'
          )            

    #moving motors backward
    mycursorbwd = mydb.cursor()    
    sql = "UPDATE motor SET driving=1,direction='BW'"
    mycursorbwd.execute(sql)
    mydb.commit() 
    mycursorbwd.close()
    mydb.close

    return "true"
