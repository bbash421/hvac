import settings
import mysql.connector


def start():
    mydb = mysql.connector.connect(
        host='localhost',
        user='bryan',
        passwd='bryan',
        database='fau'
    )

    mycursorstop = mydb.cursor()
    sql = "delete from traject;"
    mycursorstop.execute(sql)
    mydb.commit()

    sql = "update sensors set sensorfront = 0,sensorleft = 0,sensorright = 0,sensordown = 0,sensorback = 0;"
    mycursorstop.execute(sql)
    mydb.commit()

    mydb.close()

    return "true"
