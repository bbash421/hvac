import time

#driving motors
import RPi.GPIO as GPIO_LEFT
import RPi.GPIO as GPIO_RIGHT
#sensors
import RPi.GPIO as GPIO_FRONT_SENSOR
import RPi.GPIO as GPIO_LEFT_SENSOR
import RPi.GPIO as GPIO_RIGHT_SENSOR
import RPi.GPIO as GPIO_DOWN_SENSOR
import RPi.GPIO as GPIO_BACK_SENSOR
#brush
import RPi.GPIO as GPIO_BRUSH_TOP
import RPi.GPIO as GPIO_BRUSH_BOTTOM

#Front Sensor 
GPIO_FRONT_SENSOR.setwarnings(False)
GPIO_FRONT_SENSOR.setmode(GPIO_FRONT_SENSOR.BCM)
GPIO_FRONT_SENSOR_TRIGGER = 11
GPIO_FRONT_SENSOR_ECHO = 8
GPIO_FRONT_SENSOR.setup(GPIO_FRONT_SENSOR_TRIGGER, GPIO_FRONT_SENSOR.OUT)
GPIO_FRONT_SENSOR.setup(GPIO_FRONT_SENSOR_ECHO, GPIO_FRONT_SENSOR.IN)

#Left Sensor 
GPIO_LEFT_SENSOR.setwarnings(False)
GPIO_LEFT_SENSOR.setmode(GPIO_LEFT_SENSOR.BCM)
GPIO_LEFT_SENSOR_TRIGGER = 26
GPIO_LEFT_SENSOR_ECHO = 19
GPIO_LEFT_SENSOR.setup(GPIO_LEFT_SENSOR_TRIGGER, GPIO_LEFT_SENSOR.OUT)
GPIO_LEFT_SENSOR.setup(GPIO_LEFT_SENSOR_ECHO, GPIO_LEFT_SENSOR.IN)

#Right Sensor 
GPIO_RIGHT_SENSOR.setwarnings(False)
GPIO_RIGHT_SENSOR.setmode(GPIO_RIGHT_SENSOR.BCM)
GPIO_RIGHT_SENSOR_TRIGGER = 13
GPIO_RIGHT_SENSOR_ECHO = 6
GPIO_RIGHT_SENSOR.setup(GPIO_RIGHT_SENSOR_TRIGGER, GPIO_RIGHT_SENSOR.OUT)
GPIO_RIGHT_SENSOR.setup(GPIO_RIGHT_SENSOR_ECHO, GPIO_RIGHT_SENSOR.IN)

#Down Sensor 
GPIO_DOWN_SENSOR.setwarnings(False)
GPIO_DOWN_SENSOR.setmode(GPIO_DOWN_SENSOR.BCM)
GPIO_DOWN_SENSOR_TRIGGER = 16
GPIO_DOWN_SENSOR_ECHO = 12
GPIO_DOWN_SENSOR.setup(GPIO_DOWN_SENSOR_TRIGGER, GPIO_DOWN_SENSOR.OUT)
GPIO_DOWN_SENSOR.setup(GPIO_DOWN_SENSOR_ECHO, GPIO_DOWN_SENSOR.IN)

#Back Sensor 
GPIO_BACK_SENSOR.setwarnings(False)
GPIO_BACK_SENSOR.setmode(GPIO_BACK_SENSOR.BCM)
GPIO_BACK_SENSOR_TRIGGER = 18
GPIO_BACK_SENSOR_ECHO = 23
GPIO_BACK_SENSOR.setup(GPIO_BACK_SENSOR_TRIGGER, GPIO_BACK_SENSOR.OUT)
GPIO_BACK_SENSOR.setup(GPIO_BACK_SENSOR_ECHO, GPIO_BACK_SENSOR.IN)


#driving motors
MOTOR_LEFT_1 = 3
MOTOR_LEFT_2 = 2
MOTOR_RIGHT_1 = 17
MOTOR_RIGHT_2 = 27
GPIO_LEFT.setmode(GPIO_LEFT.BCM)
GPIO_LEFT.setwarnings(False)
GPIO_LEFT.setup(MOTOR_LEFT_1,GPIO_LEFT.OUT)
GPIO_LEFT.setup(MOTOR_LEFT_2,GPIO_LEFT.OUT)
GPIO_RIGHT.setmode(GPIO_RIGHT.BCM)
GPIO_RIGHT.setwarnings(False)
GPIO_RIGHT.setup(MOTOR_RIGHT_1,GPIO_RIGHT.OUT)
GPIO_RIGHT.setup(MOTOR_RIGHT_2,GPIO_RIGHT.OUT)

#brushes
BRUSH_MOTOR_TOP_1 = 7
BRUSH_MOTOR_TOP_2 = 5
BRUSH_MOTOR_BOTTOM_1 = 10
BRUSH_MOTOR_BOTTOM_2 = 9
GPIO_BRUSH_TOP.setmode(GPIO_BRUSH_TOP.BCM)
GPIO_BRUSH_TOP.setwarnings(False)
GPIO_BRUSH_TOP.setup(BRUSH_MOTOR_TOP_1,GPIO_BRUSH_TOP.OUT)
GPIO_BRUSH_TOP.setup(BRUSH_MOTOR_TOP_2,GPIO_BRUSH_TOP.OUT)
GPIO_BRUSH_BOTTOM.setmode(GPIO_BRUSH_BOTTOM.BCM)
GPIO_BRUSH_BOTTOM.setwarnings(False)
GPIO_BRUSH_BOTTOM.setup(BRUSH_MOTOR_BOTTOM_1,GPIO_BRUSH_TOP.OUT)
GPIO_BRUSH_BOTTOM.setup(BRUSH_MOTOR_BOTTOM_2,GPIO_BRUSH_TOP.OUT)

def distancefront():
  MAX_TIME = 0.04 
  GPIO_FRONT_SENSOR.output(GPIO_FRONT_SENSOR_TRIGGER, True)
  time.sleep(0.00001)
  GPIO_FRONT_SENSOR.output(GPIO_FRONT_SENSOR_TRIGGER, False)

  start = time.time()
  timeout = start + MAX_TIME
  while GPIO_FRONT_SENSOR.input(GPIO_FRONT_SENSOR_ECHO)==0 and start <= timeout:
    start = time.time()
  
  stop = time.time()
  timeout = stop + MAX_TIME
  while GPIO_FRONT_SENSOR.input(GPIO_FRONT_SENSOR_ECHO)==1 and stop <= timeout:
    stop = time.time()
  
  GPIO_FRONT_SENSOR.output(GPIO_FRONT_SENSOR_TRIGGER, False)

  elapsed = stop-start
  distance = (elapsed * 34300)/2.0
  time.sleep(0.02)
  return round(distance,1)


def distanceleft():
  MAX_TIME = 0.04 
  GPIO_LEFT_SENSOR.output(GPIO_LEFT_SENSOR_TRIGGER, True)
  time.sleep(0.00001)
  GPIO_LEFT_SENSOR.output(GPIO_LEFT_SENSOR_TRIGGER, False)

  start = time.time()
  timeout = start + MAX_TIME
  while GPIO_LEFT_SENSOR.input(GPIO_LEFT_SENSOR_ECHO)==0 and start <= timeout:
    start = time.time()
  
  stop = time.time()
  timeout = stop + MAX_TIME
  while GPIO_LEFT_SENSOR.input(GPIO_LEFT_SENSOR_ECHO)==1 and stop <= timeout:
    stop = time.time()
  
  GPIO_LEFT_SENSOR.output(GPIO_LEFT_SENSOR_TRIGGER, False)

  elapsed = stop-start
  distance = (elapsed * 34300)/2.0
  time.sleep(0.02)
  return round(distance,1)


def distanceright():
  MAX_TIME = 0.04 
  GPIO_RIGHT_SENSOR.output(GPIO_RIGHT_SENSOR_TRIGGER, True)
  time.sleep(0.00001)
  GPIO_RIGHT_SENSOR.output(GPIO_RIGHT_SENSOR_TRIGGER, False)

  start = time.time()
  timeout = start + MAX_TIME
  while GPIO_RIGHT_SENSOR.input(GPIO_RIGHT_SENSOR_ECHO)==0 and start <= timeout:
    start = time.time()
  
  stop = time.time()
  timeout = stop + MAX_TIME
  while GPIO_RIGHT_SENSOR.input(GPIO_RIGHT_SENSOR_ECHO)==1 and stop <= timeout:
    stop = time.time()
  
  GPIO_RIGHT_SENSOR.output(GPIO_RIGHT_SENSOR_TRIGGER, False)

  elapsed = stop-start
  distance = (elapsed * 34300)/2.0
  time.sleep(0.02)
  return round(distance,1)


def distancedown():
  MAX_TIME = 0.04 
  GPIO_DOWN_SENSOR.output(GPIO_DOWN_SENSOR_TRIGGER, True)
  time.sleep(0.00001)
  GPIO_DOWN_SENSOR.output(GPIO_DOWN_SENSOR_TRIGGER, False)

  start = time.time()
  timeout = start + MAX_TIME
  while GPIO_DOWN_SENSOR.input(GPIO_DOWN_SENSOR_ECHO)==0 and start <= timeout:
    start = time.time()
  
  stop = time.time()
  timeout = stop + MAX_TIME
  while GPIO_DOWN_SENSOR.input(GPIO_DOWN_SENSOR_ECHO)==1 and stop <= timeout:
    stop = time.time()
  
  GPIO_DOWN_SENSOR.output(GPIO_DOWN_SENSOR_TRIGGER, False)

  elapsed = stop-start
  distance = (elapsed * 34300)/2.0
  time.sleep(0.02)
  return round(distance,1)


def distanceback():
  MAX_TIME = 0.04 
  GPIO_BACK_SENSOR.output(GPIO_BACK_SENSOR_TRIGGER, True)
  time.sleep(0.00001)
  GPIO_BACK_SENSOR.output(GPIO_BACK_SENSOR_TRIGGER, False)

  start = time.time()
  timeout = start + MAX_TIME
  while GPIO_BACK_SENSOR.input(GPIO_BACK_SENSOR_ECHO)==0 and start <= timeout:
    start = time.time()
  
  stop = time.time()
  timeout = stop + MAX_TIME
  while GPIO_BACK_SENSOR.input(GPIO_BACK_SENSOR_ECHO)==1 and stop <= timeout:
    stop = time.time()
  
  GPIO_BACK_SENSOR.output(GPIO_BACK_SENSOR_TRIGGER, False)

  elapsed = stop-start
  distance = (elapsed * 34300)/2.0
  time.sleep(0.02)
  return round(distance,1)
