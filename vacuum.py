import settings

def vacuum_on():

	settings.GPIO_VACUUM.output(settings.VACUUM_MOTOR_1, settings.GPIO_VACUUM.HIGH)
	return 'true'

def vacuum_off():
	settings.GPIO_VACUUM.output(settings.VACUUM_MOTOR_1, settings.GPIO_VACUUM.LOW)
	return 'true'