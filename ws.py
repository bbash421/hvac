import json,os
import RPi.GPIO as GPIO_VACUUM
from stop import stop
from forward import forward
from forward_left import forward_left
from forward_right import forward_right
from backward import backward
from backward_left import backward_left
from backward_right import backward_right
from update import update
from status import status
from flask import Flask, request, render_template
from threading import Thread
from sensors import sensors
from brush import brush_on
from brush import brush_off
from start import start
from home import home


import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

tsensors = Thread(target=sensors, args=())
tsensors.start()

app = Flask(__name__,template_folder='template')

@app.route('/', methods=['GET'])
def api():
       action = request.args.get('action')
       if action == 'login':
          str = render_template('login.html')
          
        
       if action == 'sc':
          str = render_template('sc.html')
          
       if action == 'sensor_check':
          str = render_template('sc_sensors.html')
          
       if action == 'movement_check':
          str = render_template('sc_movement.html')
          
       if action == 'brush_vacuum_check':
          str = render_template('sc_brush_vacuum.html')
          
       if action == 'sc_camera':
          str = render_template('sc_camera.html')
          
       if action == 'sc_confirm':
          str = render_template('sc_confirm.html')
          
       if action == 'main':
          str =  render_template('index.html')

       if action == 'fw':
          t = Thread(target=forward, args=())
          t.start()
          str = ''

       if action == 'lf':
          t = Thread(target=forward_left, args=())
          t.start()
          str = ''

       if action == 'home':
          str = home()

       if action == 'start':
          t = Thread(target=start, args=())
          t.start()
          str = ''

       if action == 'rf':
          t = Thread(target=forward_right, args=())
          t.start()
          str = ''

       if action == 'bw':
          t = Thread(target=backward, args=())
          t.start()
          str = ''

       if action == 'lb':
          t = Thread(target=backward_left, args=())
          t.start()
          str = ''
          
       if action == 'rb':
          t = Thread(target=backward_right, args=())
          t.start()
          str = ''
          
       if action == 'update':
          str = update()

       if action == 'st':
          t = Thread(target=stop, args=())
          t.start()
          str = ''

       if action == 'status':
          str = status()

       if action == 'b1':
          t = Thread(target=brush_on, args=())
          t.start()
          str = ''

       if action == 'b0':
          t = Thread(target=brush_off, args=())
          t.start()
          str = ''

       if action == 'v1':
          GPIO_VACUUM.setmode(GPIO_VACUUM.BCM)
          GPIO_VACUUM.setup(21, GPIO_VACUUM.OUT)
          GPIO_VACUUM.output(21,GPIO_VACUUM.HIGH)
          str = ''

       if action == 'v0':
          GPIO_VACUUM.setmode(GPIO_VACUUM.BCM)
          GPIO_VACUUM.setup(21, GPIO_VACUUM.OUT)
          GPIO_VACUUM.output(21,GPIO_VACUUM.LOW)
          str = ''
          
       if action == 'shutdown':
          os.system("shutdown now");
          str = ''

       return str



if __name__ == "__main__":
   app.run(threaded=True,debug=False, host="10.1.10.202", port=80)

