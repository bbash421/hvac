import settings
import time
import mysql.connector

def status():
    mydb=mysql.connector.connect(
                host='localhost',
                user='bryan',
                passwd='bryan',
                database='fau'
    )    
    
    mycursorstatus = mydb.cursor() 
    mycursorstatus.execute("SELECT sensorfront,sensorleft,sensorright,sensordown,sensorback,(select driving from motor limit 0,1) as driving from sensors")
    myresult = mycursorstatus.fetchall()  
    
    xstr = '{ '
    xstr = xstr + ' "driving" : "'+str(myresult[0][5])+'",'
    xstr = xstr + ' "sensorfront" : "'+str(myresult[0][0])+'",'    
    xstr = xstr + ' "sensorleft" : "'+str(myresult[0][1])+'",'        
    xstr = xstr + ' "sensorright" : "'+str(myresult[0][2])+'",'
    xstr = xstr + ' "sensorback" : "'+str(myresult[0][4])+'",'
    xstr = xstr + ' "sensordown" : "'+str(myresult[0][3])+'"'    
    xstr = xstr + '}'
    
    mycursorstatus.close()
    mydb.close()


    return xstr
