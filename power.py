import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.IN)

def shutdown(channel):
    os.system("sudo shutdown now")

GPIO.add_event_detect(25, GPIO.FALLING, callback = shutdown, bouncetime = 2000)

while 1:
    time.sleep(1)